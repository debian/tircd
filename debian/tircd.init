#!/bin/sh
### BEGIN INIT INFO
# Provides:	  tircd
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Twitter IRC deamon
### END INIT INFO

ACTION="${1}"

NAME="tircd"
DESC="Twitter IRC proxy"
PIDFILE="/var/run/tircd.pid"
LOGFILE="/var/log/tircd/tircd.log"
BINARY="/usr/bin/tircd"

test -x ${BINARY} || exit 0

. /lib/lsb/init-functions

ENABLED=0

if [ -f /etc/default/tircd ]; then
	. /etc/default/tircd
fi

case "${ACTION}" in
	start|start-now)
		if [ "${ENABLED}" != "1" ]; then
			exit 0
		fi

		log_daemon_msg "Starting ${DESC}" ${NAME}
		if start-stop-daemon --start -q --oknodo --pidfile ${PIDFILE} \
			--make-pidfile --chuid tircd --user tircd --name tircd \
			--background --startas ${BINARY}
		then
			log_end_msg 0
		else
			log_end_msg 1
		fi
		;;
	stop|stop-now)
		log_daemon_msg "Stopping ${DESC}" ${NAME}
		if start-stop-daemon --stop --quiet --pidfile ${PIDFILE} \
			--retry 10 2>&1 >>${LOGFILE}
		then
			rm -f ${PIDFILE}
			log_end_msg 0
		else
			log_end_msg 1
		fi
		;;
	restart|force-reload)
		${0} stop
		sleep 1
		${0} start
		;;
	*)
		echo "Usage: /etc/init.d/${NAME} {start|stop|restart|force-reload}"
		exit 1
		;;
esac

exit 0

