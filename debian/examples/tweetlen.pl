# tweetlen - keep track of the length of your tweets
# --------------------------------------------------
#
# This script provides a statusbar item (called 'tweetlen') that, when
# you are typing in #twitter, displays how many characters you have
# left out of Twitter's 140-character limit. If you have fewer than 20
# characters left, the item is highlighted in magenta, and if there
# are fewer than 10, red.
#
# To add it to your statusbar, load this script and then type
# "/statusbar window add -alignment right tweetlen". (Replace the
# -alignment with whatever suits your taste.)

# Copyright (c) 2009, Decklin Foster <decklin@red-bean.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use Irssi;
use Irssi::TextUI;
use strict;

our %IRSSI = (
    authors     => 'Decklin Foster',
    contact     => 'decklin@red-bean.com',
    name        => 'tweetlen',
    description => 'Update length of tweet in statusbar',
    license     => 'ISC',
);

Irssi::statusbar_item_register('tweetlen', undef, 'tweetlen_sb');
Irssi::signal_add_last('gui key pressed', 'key_pressed');

sub key_pressed
{
    Irssi::statusbar_items_redraw('tweetlen');
}

sub tweetlen_sb
{
    my ($item, $size_only) = @_;

    my @witems = Irssi::active_win->items;
    my $text = "";

    if (@witems && $witems[0]->{name} eq "#twitter") {
        my $chars = 140 - length(Irssi::parse_special("\$L"));

        if ($chars < 10) {
            $chars = "%R$chars%n";
        } elsif ($chars < 20) {
            $chars = "%M$chars%n";
        }

        $text = "{sb $chars}";
    }

    $item->default_handler($size_only, $text, undef, 1);
}
